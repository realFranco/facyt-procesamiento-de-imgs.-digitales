/*
Devs: 
	- f97gp1@gmail.com
	- oliveiragabr@outlook.com

Date: March 11st, 2020

Back into C Language since 3 years ago.

To open the gimp inferface.

$: flatpak run org.gimp.GIMP//stable to open gimp
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h> // Round


void readImg(char [], int **, int *, int *);

void invImg(char [], int **, int, int);

void grayScale(char [], int **, int, int, int);

int *imgHistogram(char [], int **, int, int);

void imgDecreaseDimm(char [], int **, int, int, int);

void flipImage(char [], int **, int, int, int);

int accum(int *, int);

void equalizer(char [], int **, int*, int, int);

char *create1DStr(int);

void readImgInf(char [], int *, int *);

char *outFileRoute(char *, char *, char *);

void createInf(char *, int, int);


int main(int argc, char const *argv[]) {

	int *hist;
	char *temp[ 4 ], * _t_route;
	int _args = 0, row = 0, columns = 0;
	
	for(int i = 0; i < argc; i++){
		// printf("%s\n", argv[i]);
		if ((char *) argv[i] != NULL){
			temp[_args] = (char *) argv[i];
			_args += 1;
		}
	}

	if( _args > 1){
		
		readImgInf(temp[1], &row, &columns);

		int *arr[row]; // Ugly hack
		for (int i=0; i<row; i++)
			arr[i] = (int *) malloc(columns * sizeof(int));

    	readImg( temp[1], arr, &row, &columns );

		_t_route = outFileRoute(temp[1], "_n.", temp[2]);
		invImg(_t_route, arr, row, columns);
		createInf(outFileRoute(temp[1], "_n.inf", ""), row, columns);

		// vertical flip
		_t_route = outFileRoute(temp[1], "_fv.", temp[2]);
		flipImage(_t_route, arr, row, columns, 1);
		createInf(outFileRoute(temp[1], "_fv.inf", ""), row, columns);

		// horizontal flip
		_t_route = outFileRoute(temp[1], "_fh.", temp[2]);
		flipImage(_t_route, arr, row, columns, 0);
		createInf(outFileRoute(temp[1], "_fh.inf", ""), row, columns);

		// big decrease -> less pixels
		_t_route = outFileRoute(temp[1], "_mt.", temp[2]);
		imgDecreaseDimm(_t_route, arr, row, columns, 50);
		createInf(outFileRoute(temp[1], "_mt.inf", ""), row, columns);

		_t_route = outFileRoute(temp[1], "_4n.", temp[2]);
		grayScale(_t_route, arr, 4, row, columns);
		createInf(outFileRoute(temp[1], "_4n.inf", ""), row, columns);

		_t_route = outFileRoute(temp[1], "_16.", temp[2]);
		grayScale(_t_route, arr, 16, row, columns);
		createInf(outFileRoute(temp[1], "_16.inf", ""), row, columns);

		_t_route = outFileRoute(temp[1], "_h.txt", "");
		hist = imgHistogram(_t_route, arr, row, columns);

		_t_route = outFileRoute(temp[1], "_eq.", temp[2]);
		equalizer(_t_route, arr, hist, row, columns);
		createInf(outFileRoute(temp[1], "_eq.inf", ""), row, columns);
	}
	else{
		printf("Not existing variables to catch.\n");
		exit(1);
	}

	return 0;
}


void readImgInf(char input[], int *row, int *columns){
	
	FILE *fp_inf;
	unsigned char ch;
	char *inf_input, input_len;

	input_len = strlen(input);

	inf_input = create1DStr(input_len);

	strcpy(inf_input, input);
	
	strcat(inf_input, ".inf");

	fp_inf = fopen(inf_input, "r");
	
	if(fp_inf != NULL){

		fscanf(fp_inf, "%d %d", columns, row);
		fclose(fp_inf);
	}
	else{
		printf("Files not found.\n");
		exit(1);
	}
}


void readImg(char input[], int **im, int *row, int *columns){
	
	FILE *fp;
	unsigned char ch;
	char *data_input, input_len;

	input_len = strlen(input);

	data_input = create1DStr(input_len);

	strcpy(data_input, input);
	strcat(data_input, ".data");

	fp = fopen(data_input, "rb");
	
	if(fp != NULL){

		for(int i=0; i < *row; i++){
			for(int j=0; j < *columns; j++){
				fread(&ch, sizeof(ch), 1, fp);
				*(*(im+i)+j) = (int) ch;
			}
		}

		fclose(fp);
	}
	else{
		printf("File not found.\n");
		exit(1);
	}
}


/*Inverting the color of the image*/
void invImg(char input[], int **im, int row, int columns){
	
	FILE *fw;
	unsigned char ch;

	fw = fopen(input, "wb");
	if( fw != NULL){
		
		for(int i = 0; i < row; i++){
			for(int j = 0; j < columns; j++){
				ch = ~*(*(im+i)+j);
				fwrite(&ch, sizeof(ch), 1, fw);
			}
		}
	}
	else{
		printf("Problem writing the output file.");
		exit(1);
	}

	fclose(fw);
}


/* Filter the image colors into a gray scales only*/
void grayScale(char input[], int **im, int grayscale, int row, int columns){
	
	FILE *fw;
	unsigned char ch;
	int set;

	set = 256 / grayscale;
	fw = fopen(input, "wb");
	if( fw != NULL){
		for(int i = 0; i < row; i++){
			for(int j = 0; j < columns; j++){
				ch =  round( ( grayscale * *(*(im+i)+j) ) / 256) * set;
				fwrite(&ch, sizeof(ch), 1, fw);
			}
		}
	}
	else{
		printf("Problem writing the output file.");
		exit(1);
	}

	fclose(fw);
}


/* Generating an histogram of colors ocurrences */
int * imgHistogram(char input[], int **im, int row, int columns){
	
	FILE *fw;
	int *hist = NULL;
	
	// Zero at every array's position
	hist = (int *) calloc(255, sizeof(int));

	for(int i = 0; i < row; i++)
		for(int j = 0; j < columns; j++)
			hist[ *(*(im+i)+j) ]++;
	
	fw = fopen(input, "wb");
	if( fw != NULL){

		for(int _ = 0; _ < 256; _++)
			fprintf (fw, "%d %d\n", _, hist[_]);

		fclose(fw);
	}
	else{
		printf("Problem writing the output file.");
		exit(1);
	}	

	return hist;
}


void imgDecreaseDimm(char input[], int **im, int row, int columns, int dim){
	
	FILE *fw;
	float _t;
	unsigned char ch;

	fw = fopen(input, "wb");
	if( fw != NULL){
		dim = 100 - dim;
		_t = round( 100 / dim );

		for(int i=0; i < row; i += _t){
			for(int j=0; j < columns; j += _t){
				ch = *(*(im+i)+j);
				fwrite(&ch, sizeof(ch), 1, fw);
			}
		}

		// printf("%s > %d x %d\n", input,
		// 	(int) round(columns / _t),
		// 	(int) round(row / _t));

		fclose(fw);
	}
	else{
		printf("Problem writing the output file.");
		exit(1);
	}
}


/* Flip an img. */
void flipImage(char input[], int **im, int row, int columns, int vertical){
	
	FILE *fw;
	int *arr[row];
	unsigned char ch;


	for (int i=0; i<row; i++)
		arr[i] = (int *) malloc(columns * sizeof(int));

	fw = fopen(input, "wb");
	if( fw != NULL){
		for(int i = 0; i < row; i++){
			for(int j = 0; j < columns; j++){
				if(vertical == 1){ 
					arr[i][j] = im[row - i][j];
				}
				else{
					arr[i][j] = im[i][columns - j];
				}
				ch = arr[i][j];
				fwrite(&ch, sizeof(ch), 1, fw);
			}
		}
	}
	else{
		printf("Problem writing the output file.");
		exit(1);
	}

	fclose(fw);
}


int accum(int *hist, int limit){
	int out = 0;

	for(int i = 0; i < limit; i++)
		out += hist[i];
	
	return out;
}


// Ref > https://en.wikipedia.org/wiki/Histogram_equalization
void equalizer(char input[], int **im, int *hist, int row, int columns){
	FILE *fw;
	unsigned char ch;
	int q_0 = 0, q_k = 256;
	int *cdf_container = NULL, cdf = 0, cdf_min = hist[0]; // cdf_min
	
	cdf_container = (int *) calloc(256, sizeof(int));
	for( int i=0; i < 256; i++)
		cdf_container[i] = accum(hist, i);

	fw = fopen(input, "wb");
	if( fw != NULL){
		for(int i = 0; i < row; i++){
			for(int j = 0; j < columns; j++){
				cdf = cdf_container[ im[i][j] ]; /// [0, 255] 
				ch = round( 
					255 * 
					(cdf - cdf_min) / ((row * columns) - cdf_min)
				);

				fwrite(&ch, sizeof(ch), 1, fw);
			}
		}
	}
	else{
		printf("Problem writing the output file.");
		exit(1);
	}

	fclose(fw);
}

char *create1DStr(int n){
	return (char *) calloc(n, sizeof(char));
}


/* This function will create a char's array,
returning a relative route + extension of a file


route = file
sub_str = _n.
extension = ext

output = file_n.ext
*/
char *outFileRoute(char *route, char *sub_str, char *extension){
	int n = 0;
	char *output;

	n += strlen(route);
	n += strlen(sub_str);
	n += strlen(extension);

	output = create1DStr(n);

	strcpy(output, route); 
	strcat(output, sub_str); 
	strcat(output, extension);

	return output;
}


/*
This function will create an output file
*/
void createInf(char *name, int row, int columns){
	FILE *fo;
	char *output;

	output = create1DStr( 100 );
	
	fo = fopen(name, "wb");
	if(fo != NULL){
		// columns, row
		sprintf(output, "%d %d\n", columns, row);
		fwrite(output, sizeof(output), 1, fo);

		
	}
	else{
		printf("Outfile not found.\n");
		exit(1);
	}

	fclose(fo);
}
