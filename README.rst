=======================
FaCyT - Procesamiento de Imágenes
=======================

.. image:: https://img.shields.io/badge/GCC%20version-7.4.0-%23555555?style=for-the-badge&logo=appveyor

===========
Descripción - Asignación #1
===========

Editor de imágenes. Dada una imagen en archivo .raw, convertirla a .data y generar:

1. Una imagen con colores invertidos.
2. Flip vertical y horizontal de la imagen
3. Decrementar un 50% el tamaño de la imagen original.
4. Imagen con 16 escalas de grises y otra con 4 escalas.
5. Archivo de texto, en formato .txt, con un histograma, mostrando la ocurrencia de los 256 niveles de grises.
6. Equalización de la imagen.

=========
Ejemplos
=========

Ejemplo de entrada:

.. image:: https://fgil1.s3-us-west-2.amazonaws.com/facyt/imgs-digs/caballo/caballo.png


Ejemplos de salida:

.. image:: https://fgil1.s3-us-west-2.amazonaws.com/facyt/imgs-digs/caballo/caballo_n.png

.. image:: https://fgil1.s3-us-west-2.amazonaws.com/facyt/imgs-digs/caballo/caballo_16n.png

.. image:: https://fgil1.s3-us-west-2.amazonaws.com/facyt/imgs-digs/caballo/caballo_fh.png

.. image:: https://fgil1.s3-us-west-2.amazonaws.com/facyt/imgs-digs/caballo/caballo_eq.png


=============
Requerimentos Generales.
=============

Compiladores en C / C++.

============
Instrucciones Generales.
============

1. Clonar el repositorio.


2. Añadir a la raiz de la carpeta actual una nueva carpeta, con el nombre del archivo fuente

.. code-block:: console

   $: mkdir file_name
   
3. Incluir el archivo '.raw' y '.inf' a procesar dentro de la carpeta creada

4. Modficar la extensión del archivo, de .raw a .data

.. code-block:: console
   
   $: cp file_name.raw file_name.data

2. Compilar el archivo fuente, hay dos de ellos (test.c / test.cpp).

.. code-block:: console

   # Ejemplo de compilación en C
   # Añada el flag -lm para enlazar la librería math.h
   $: gcc test -o run -lm
   
   # Ejemplo de compilación en C++
   $: g++ test.cpp -o run
   
3. Ejecutar el objeto

.. code-block:: console
   
   # Se le deben añadir dos argumentos en la línea de ejecución
   # Dicho argumento estará compuesto por la carpeta que aloja los archivos
   # '.inf' & '.data'
   # Para el caso de prueba escribir 'caballo/caballo'
   
   $: ./run caballo/caballo raw
   
   # Eso indicará que el archivo 'caballo.inf' y 'caballo.data'
   # a analizar estarán dentro de la carpeta 'caballo'.
   #
   # La segunda cadena indica la extensión que tendrán las imágenes resultantes
   # puede establecerse a 'raw' o 'data'.
   
   
